import "./App.css";
import Weather from "./Components/weather";
import Heading from "./Components/Heading";

function App() {
  return (
    <div className="App">
      <Heading />
      <Weather />
    </div>
  );
}

export default App;
