import React, { Component } from "react";
import "../Weather.css";

export class Heading extends Component {
  render() {
    return <h1 className="mainHeading">3-Hourly Weather Forecast, <br/>Bangalore</h1>;
  }
}

export default Heading;
