import React, { Component } from "react";
import "../Weather.css";

class Weather extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDataFetched: false,
      myData: null,
    };
  }

  componentDidMount() {
    fetch(
      "https://api.openweathermap.org/data/2.5/forecast?lat=12.934533&lon=77.626579&cnt=6&appid=01e2a71059ec9f4d231db826f23b6f1c"
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.cod === "200") {
          this.setState({
            isDataFetched: true,
            myData: data.list,
          });
        }
      })
      .catch((err) => console.console.error(err.message));
  }

  render() {
    if (this.state.myData != null) {
      let reqData = this.state.myData.map((eachData, index) => {
        return {
          iconNeeded: eachData.weather[0].icon,
          id: index,
          tempMax: (eachData.main.temp_max - 273).toFixed(1),
          tempMin: (eachData.main.temp_min - 273).toFixed(1),
          dateNeeded: eachData.dt_txt.split(" "),
        };
      });
      return (
        <div className="mainDiv">
          {reqData.map((tempValue) => {
            let dateTobeStructured = tempValue.dateNeeded;
            let extractedDate = dateTobeStructured[0];
            let extractedTime = dateTobeStructured[1];

            let formattedDate = new Date(extractedDate);
            let myDate =
              formattedDate.getDate() +
              "/" +
              (formattedDate.getMonth() + 1) +
              "/" +
              formattedDate.getFullYear();
            return (
              <div className="eachDayContainer" key={tempValue.id}>
                <h4 className="weekdayName">{myDate}</h4>
                <h4 className="weekdayName">{extractedTime}</h4>
                <div className="imageStyling">
                  <img
                    src={`http://openweathermap.org/img/wn/${tempValue.iconNeeded}.png`}
                    alt="weather"
                  />
                </div>
                <div className="temperatureDiv">
                  <p className="highestTemp">{tempValue.tempMax}&deg;</p>
                  <p className="lowestTemp">{tempValue.tempMin}&deg;</p>
                </div>
              </div>
            );
          })}
        </div>
      );
    }
  }
}

export default Weather;
